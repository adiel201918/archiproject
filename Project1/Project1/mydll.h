#ifndef SIMPLEDLL_H
#define SIMPLEDLL_H

#include <Windows.h>

#ifdef SIMPLEDLL_EXPORTS
#define SIMPLEDLL_API __declspec(dllexport)
#else
#define SIMPLEDLL_API __declspec(dllimport)
#endif

extern "C" SIMPLEDLL_API void showMessage();

#endif
