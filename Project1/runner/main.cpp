#include <Windows.h>
#include <iostream>

int main()
{
    // Get the process ID of the target process (notepad.exe in this case)
    DWORD processId = 0;
    HWND notepadHandle = FindWindow(NULL, L"Untitled - Notepad");
    if (notepadHandle == NULL)
    {
        std::cout << "Failed to find notepad window.\n";
        return 1;
    }
    GetWindowThreadProcessId(notepadHandle, &processId);

    // Open a handle to the target process
    HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processId);
    if (processHandle == NULL)
    {
        std::cout << "Failed to open process handle.\n";
        return 1;
    }

    // Allocate memory in the target process to store the DLL path
    LPVOID dllPathAddress = VirtualAllocEx(processHandle, NULL, MAX_PATH, MEM_COMMIT, PAGE_READWRITE);
    if (dllPathAddress == NULL)
    {
        std::cout << "Failed to allocate memory in target process.\n";
        return 1;
    }

    // Write the path of the DLL to the allocated memory in the target process
    const char* dllPath = R"(C:\Users\adiel the hackerman\OneDrive\Files\ComputerScience\magshimim\2022-23 magshimim\a\archi\week 17\home\windows\Project1\x64\Debug\Project1.dll)";
    if (!WriteProcessMemory(processHandle, dllPathAddress, dllPath, strlen(dllPath) + 1, NULL))
    {
        std::cout << "Failed to write DLL path to target process.\n";
        return 1;
    }

    // Get the address of the LoadLibraryA function in the kernel32.dll module
    HMODULE kernel32 = GetModuleHandleA("kernel32.dll");
    if (kernel32 == NULL)
    {
        std::cout << "Failed to get kernel32.dll module handle.\n";
        return 1;
    }
    LPVOID loadLibraryAddress = (LPVOID)GetProcAddress(kernel32, "LoadLibraryA");
    if (loadLibraryAddress == NULL)
    {
        std::cout << "Failed to get LoadLibraryA address.\n";
        return 1;
    }

    // Create a remote thread in the target process to load the DLL
    HANDLE remoteThread = CreateRemoteThread(processHandle, NULL, NULL, (LPTHREAD_START_ROUTINE)loadLibraryAddress, dllPathAddress, NULL, NULL);
    if (remoteThread == NULL)
    {
        std::cout << "Failed to create remote thread.\n";
        return 1;
    }

    // Wait for the remote thread to finish
    WaitForSingleObject(remoteThread, INFINITE);

    // Free the memory allocated in the target process
    VirtualFreeEx(processHandle, dllPathAddress, 0, MEM_RELEASE);

    return 0;
}
